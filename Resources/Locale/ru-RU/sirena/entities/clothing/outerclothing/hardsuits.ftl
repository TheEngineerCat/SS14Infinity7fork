ent-ClothingOuterHardsuitSyndieNoIdentification = { ent-ClothingOuterHardsuitSyndie }
    .desc = { ent-ClothingOuterHardsuitSyndie.desc }
    .suffix = Без системы идентификации
ent-ClothingOuterHardsuitMedicNoIdentification = { ent-ClothingOuterHardsuitMedic }
    .desc = { ent-ClothingOuterHardsuitMedic.desc }
    .suffix = Без системы идентификации
ent-ClothingOuterHardsuitSyndieEliteNoIdentification = { ent-ClothingOuterHardsuitSyndieElite }
    .desc = { ent-ClothingOuterHardsuitSyndieElite.desc }
    .suffix = Без системы идентификации
ent-ClothingOuterHardsuitSyndieCommanderNoIdentification = { ent-ClothingOuterHardsuitSyndieCommander }
    .desc = { ent-ClothingOuterHardsuitSyndieCommander.desc }
    .suffix = Без системы идентификации
ent-ClothingOuterHardsuitJuggernautNoIdentification = { ent-ClothingOuterHardsuitJuggernaut }
    .desc = { ent-ClothingOuterHardsuitJuggernaut.desc }
    .suffix = Без системы идентификации

ent-ClothingOuterHardsuitCentCom = Скафандр центрального командования
    .desc = Специальный скафандр для сотрудников центрального командования, легкий но также и прочный.

ent-ClothingHeadHelmetHardsuitCentCom = Шлем скафандра центрального командования
    .desc = Армированый шлем скафандра для сотрудников центрального командованияю
